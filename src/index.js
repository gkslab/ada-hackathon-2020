import 'bootstrap/dist/css/bootstrap.min.css';
import './styles.scss';
import AdaWidgetSDK from "@ada-support/ada-widget-sdk";

const widgetSDK = new AdaWidgetSDK();

const containerElement = document.getElementById("widget-container");
const templateElement = document.getElementById("template");
const buttonTemplateElement = document.getElementById("template-button");
const actionsElement = document.getElementById("actions");

const sdkInputElement = document.getElementById("widget-input-data");
const inputElement = document.getElementById("input-field");
const userNameElement = document.getElementById("username");
const passwordElement = document.getElementById("password");
const showMoreButton = document.getElementById("show-more-button");
const showPreviousButton = document.getElementById("show-previous-button");
const exitButtonElement = document.getElementById("exit-button");
const showMessageElement = document.getElementById("message");
let init = false;


const Http = new XMLHttpRequest();
let offset = 0;
let maxNumberElementsToRender = 3;
let startIndex = 0;
let endIndex = maxNumberElementsToRender;

let masterList = [];
let actionsArray = [];

function showMessage(msg) {
  showMessageElement.innerText = msg;
}


function sendResult(resultCode, refId, msg) {
  
  if(refId === undefined)
  {
    refId = "";
  }

  if(msg === undefined)
  {
    msg = "";
  }

  showMessage(msg);
  widgetSDK.sendUserData({
   resultCode,
   refId,
   msg
  }, (event) => {
    if (event.type === "SEND_USER_DATA_SUCCESS") {
      showMessage ("Data was successfully submitted");
      showMoreButton.disabled = true;
      exitButtonElement.disabled = true;
    } else {
      showMessage("Data submission failed, please try again");
    }
  });
}

exitButtonElement.onclick = (e) => {
  e.preventDefault();
  
  // disable the container so nothing else can be clicked
  disableWidget();
  sendResult("EXIT");
};

showMoreButton.onclick = (e) => {
  e.preventDefault();
  
  showMessage("Loading ...");
  renderView(endIndex + 1);
  showMessage("");
};

showPreviousButton.onclick = (e) => {
  e.preventDefault();
  
  // console.log("Reset offset ", offset - maxNumberElementsToRender);
  showMessage("Loading ...");
  renderView(startIndex - maxNumberElementsToRender);
  showMessage("");
};

widgetSDK.init((event) => {
  if (!widgetSDK.widgetIsActive) {
    containerElement.innerHTML = "The widget is not active";
    return;
  }

  console.log("initialized init", init);
  if(init) {
    return;
  } else {
    init = true;
  }

  // console.log("APP MetaData", widgetSDK.metaData);
  const { actions, source_url, secret_key_header, secret_key_value, max_per_view } = widgetSDK.metaData;
  // console.log("actions sourceUrl, secretKeyHeader, secretKeyValue: ", actions, source_url, secret_key_header, secret_key_value);

  if(max_per_view !== undefined) {
    maxNumberElementsToRender = Math.min(max_per_view, 3);
  }
  else {
    maxNumberElementsToRender = 3;
  }

 
  // capture actions that the user passed in
  // this will be rendered as buttons on each element
  if (actions !== undefined && actions.length > 0) {
    
    /**
     * BUG: Had to add this check because the init event was being fired twice?
     * TODO 
     *  */ 
    if(actionsArray.length !== actions.split(",").length){
      actions.split(",").forEach(act => {
        actionsArray.push(act.trim());
      });
    }
  }

  // get items from the source url passed in
  getData(source_url, secret_key_header, secret_key_value, offset);
  
  sdkInputElement.innerHTML = inputdata;
  init = true;
});

function getData(url, secretKeyHeader, secretKey, offset) {
  Http.open("GET", url);
  Http.setRequestHeader(secretKeyHeader, secretKey);
  Http.send();
}

function disableWidget() {
  containerElement.disabled = true;
}

function clearCardElementsInContainer() {
  // clear out all elements in the container element that are not of id template
  var cardElements = containerElement.getElementsByClassName("card");
  cardElements.forEach(element => {
    element.remove();
  });
}

function onActionClick(e) {
  e.preventDefault();
  // extract item id and action name from element id
  var elementId = document.getElementById(e.target.id).id;
  var actionInfo = elementId.split("-");
  sendResult(actionInfo[1], actionInfo[0]);
}

/** 
 * Renders the data passed in to the widget as elements 
 * 
*/
function renderView(fromIndex) {
    
    // console.log("rendering view for list, from, to", masterList, fromIndex, fromIndex + maxNumberElementsToRender);
    showMoreButton.hidden = true;
    showPreviousButton.hidden = true;

    if(fromIndex < 0) {
      startIndex = 0;
    }
   
    var data = masterList.slice(fromIndex, fromIndex + maxNumberElementsToRender);
    startIndex = masterList.indexOf(data[0]);
    endIndex = masterList.indexOf(data.slice(-1)[0]);

    // console.log("filtered data: ", data);
    clearCardElementsInContainer();
    clearCardElementsInContainer(); // BUG! TODO have to call this twice other wise there is a remnant element. WTF.

    // load items from list
    data.forEach((item, index) => {
      // console.log("item ", item);
      var newelement = templateElement.cloneNode(true);
      containerElement.appendChild(newelement);
      newelement.hidden = false;
      if("title" in item)
      { 
        newelement.getElementsByClassName("card-title")[0].innerHTML = item["title"];
      }
      else {
        newelement.getElementsByClassName("card-title")[0].innerHTML = "Item";
      }

      if("description" in item) {
        newelement.getElementsByClassName("card-text")[0].innerHTML = item["description"];
      }
      else {
        newelement.getElementsByClassName("card-text")[0].innerHTML = "No description available";
      }

      if("image" in item) {
        newelement.getElementsByClassName("image")[0].setAttribute("src", item["image"]);
      }
      else {
        newelement.getElementsByClassName("image")[0].hidden = true;
      }

      var itemId = "";
      if("id" in item) {
        itemId = item["id"];
      }
      else {
        itemId = index.toString();
      }
    
      // add actions
      buttonTemplateElement.hidden = true; // hide root template in root element first otherwise it gets cloned

      const buttonToolbarElement = newelement.getElementsByClassName("btn-toolbar")[0];
      if(actionsArray.length > 0) {
        // console.log("rendering actions");
        // render the buttons
        actionsArray.forEach(action => {
          var newAction = buttonTemplateElement.cloneNode(true);
          buttonToolbarElement.appendChild(newAction);
          newAction.id = itemId + "-" + action;
          newAction.hidden = false;
          newAction.innerHTML = action;
          newAction.onclick = onActionClick;
          
        });
    
      }
    });

    // update the navigation based on the offset
    console.log("evaluating rules for masterlist length, current offset", masterList.length, offset);
    if(startIndex >= maxNumberElementsToRender && endIndex < masterList.length) {
      showPreviousButton.hidden = false;
    }

    if(endIndex >= masterList.length) {
      showPreviousButton.hidden = false;
      showMoreButton.hidden = true;
    }
    if (endIndex < masterList.length - 1) {
      showMoreButton.hidden = false;
    }

    // hide templates
    templateElement.hidden = true;
    buttonTemplateElement.hidden = true;
}


/** 
 * Event listener for Http Request to retrieve the list of data to render 
 * */
Http.onreadystatechange = (e) => {
  if(Http.readyState === XMLHttpRequest.DONE) {
    console.log("APP Source Url Returned: ", Http.responseText);
    if(Http.responseText === undefined)
    {  
      return;
    }
    
    try {
      var response = JSON.parse(Http.responseText);
      if("data" in response) {
        masterList = response.data;
        renderView(offset);
      }
    }
    catch (err) {
      console.log(err);
    }
  }
  
}


